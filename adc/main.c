#define BAUD 38400
#define ADC_PS 128
#define ADC_REFV 1 /* AVCC */
#define ADC_CH 6

#include "USART.h"
#include "ADC.h"
#include "Stdio.h"
#include <avr/io.h>
#include <util/delay.h>

static USART u;

#define USART_CREATE() USART_Create(&UCSR0A, &UCSR0B, &UCSR0C, &UBRR0L, &UBRR0H, &UDR0)

int main(void)
{
	uint16_t adc_res;
	char s[10];

	u = USART_CREATE();
	USART_SetBaudRate(u, BAUD, F_CPU);
	USART_Enable(u, 1);
	USART_Puts(u, "USART\r\n");

	ADC_Create(&ADMUX, &ADCSRA, &ADCH, &ADCL);
	ADC_SetPrescaler(ADC_PS);
	ADC_SetReferenceVoltage(ADC_REFV);
	ADC_SetChannel(ADC_CH);
	ADC_Enable(1);
	USART_Puts(u, "ADC\r\n");

	while (1)
	{
		ADC_StartConversion();
		while (!ADC_IsConversionComplete())
			;
		adc_res = ADC_GetResult();
		itoa(adc_res, s, 10);
		USART_Puts(u, s);
		USART_Puts(u, ".\r\n");
		_delay_ms(500);
	}
}
