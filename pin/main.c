/* Written for Rainbowduino (ATmega328p at 16MHz
 *
 * Mirror the input from one pin to the output of another pin.
 * This is done by having a push-button connected to one pin,
 * and a LED to the other one.
 */

#include "Pin.h"
#include <avr/io.h>

int main(void)
{
	uint8_t btn1_state;
	uint8_t btn2_state;
	Pin pin_led1 = Pin_Create(0, &DDRC, &PORTC, &PINC);
	Pin pin_led2 = Pin_Create(1, &DDRC, &PORTC, &PINC);
	Pin pin_btn1 = Pin_Create(2, &DDRC, &PORTC, &PINC);
	Pin pin_btn2 = Pin_Create(3, &DDRC, &PORTC, &PINC);
	Pin_SetDirection(pin_led1, 1);			/* LED pin is an input but we want the LED to be off by default */
	Pin_Write(pin_led1, 1);
	Pin_SetDirection(pin_led2, 1);
	Pin_Write(pin_led2, 1);
	Pin_SetDirection(pin_btn1, 0);			/* Configure Push-Button pin as input (active-low) */
	Pin_SetDirection(pin_btn2, 0);
#if 1
	while (1) {
		btn1_state = Pin_Read(pin_btn1);		/* Read Push-Button pin */
		Pin_Write(pin_led1, btn1_state);

		btn2_state = Pin_Read(pin_btn2);
		Pin_Write(pin_led2, btn2_state);
	}
#endif
	return 0;
}
