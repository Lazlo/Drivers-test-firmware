#define BAUD 38400

#define PIN_SPI_SCK		7
#define PIN_SPI_MOSI	5
#define PIN_ENC_CS		4
#define PIN_ENC_INT		2

#define ETH_MAX_FRAME_LEN 1518

#include "USART.h"
#include "Pin.h"
#include "SPI.h"
#include "Enc28J60_IO.h"
#include "Enc28J60_Registers.h"
#include "Enc28J60.h"
#include "Stdio.h"
#include <avr/io.h>
#include <util/delay.h>

#if defined(__AVR_ATmega8__) || defined(__AVR_ATmega32__)
#define UCSR0A	UCSRA
#define UCSR0B	UCSRB
#define UCSR0C	UCSRC
#define UBRR0L	UBRRL
#define UBRR0H	UBRRH
#define UDR0	UDR
#endif

static USART u;
static Pin led[3];
static Pin enc28j60_cs_pin;
static Pin enc28j60_int_pin;
static Enc28J60_IO eth_io;
static Enc28J60 eth;

static void dbg(const char *label, const uint8_t data)
{
	char str[4];
	itoa(data, str, 16);
	USART_Puts(u, label);
	USART_Puts(u, " 0x");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");
}

static void USART_Init(const uint32_t baud)
{
	u = USART_Create(&UCSR0A, &UCSR0B, &UCSR0C, &UBRR0L, &UBRR0H, &UDR0);
	USART_SetBaudRate(u, baud, F_CPU);
	USART_Enable(u, 1);
	USART_Puts(u, "USART\r\n");
}

static void LED_Init(void)
{
	uint8_t i;

	led[0] = Pin_Create(2, &DDRD, &PORTD, &PIND);
	led[1] = Pin_Create(4, &DDRD, &PORTD, &PIND);
	led[2] = Pin_Create(6, &DDRD, &PORTD, &PIND);

	for (i = 0; i < 3; i++) {
		Pin_SetDirection(led[i], 1);
		Pin_Write(led[i], 1);
	}
	USART_Puts(u, "LED\r\n");
}

static void spi_master_pins_init(void)
{
	Pin spi_sck;
	Pin spi_mosi;
	spi_sck = Pin_Create(PIN_SPI_SCK, &DDRB, &PORTB, &PINB);
	spi_mosi = Pin_Create(PIN_SPI_MOSI, &DDRB, &PORTB, &PINB);
	Pin_SetDirection(spi_sck, 1);
	Pin_SetDirection(spi_mosi, 1);
}

static void SPI_Master_Init(void)
{
	spi_master_pins_init();

	SPI_Create(&SPCR, &SPSR, &SPDR);
	SPI_SetMasterMode(1);
	SPI_SetClockRate(2);

	/* ENC28J60 specific SPI mode settings
	 * These are the defaults for the AVR but we set them anyways
	 * because the ENC28J60 requires them to work. */
	SPI_SetDataOrder(0);		/* MSB first */
	SPI_SetClockPolarity(0);	/* SCK low when idle */
	SPI_SetClockPhase(0);		/* sample on rising edge of SCK */

	SPI_Enable(1);
	USART_Puts(u, "SPI\r\n");
}

static void enc28j60_create(void)
{
	enc28j60_cs_pin = Pin_Create(PIN_ENC_CS, &DDRB, &PORTB, &PINB);
	enc28j60_int_pin = Pin_Create(PIN_ENC_INT, &DDRB, &PORTB, &PINB);

	/* TODO figure out the default level of the interrupt pin */
	/* TODO check if we need to disable the internal pull-up of the MCU */
	/* TODO define interrupt routine for external interrupt */

	eth_io = Enc28J60_IO_Create(enc28j60_cs_pin);
	eth = Enc28J60_Create(enc28j60_cs_pin);
	USART_Puts(u, "ETH\r\n");
}

/* common registers */
enum {
	EIE = 0x1B,
	EIR = 0x1C,
	ECON2 = 0x1E,
};

static void print_charntimes(const char c, const uint8_t ntimes)
{
	uint8_t i;
	for (i = 0; i < ntimes; i++)
		USART_Putc(u, c);
}

static void print_separatorLine(void)
{
	print_charntimes('-', 72);
	USART_Puts(u, "\r\n");
}

static void enc28j60_show_receptionEnable(void)
{
	USART_Puts(u, "eth RXEN ");
	if (Enc28J60_GetReceptionEnable(eth))
		USART_Putc(u, '1');
	else
		USART_Putc(u, '0');
	USART_Puts(u, "\r\n");
}

static void enc28j60_show_macReceptionEnable(void)
{
	USART_Puts(u, "eth MARXEN ");
	if (Enc28J60_GetMacReceptionEnable(eth))
		USART_Putc(u, '1');
	else
		USART_Putc(u, '0');
	USART_Puts(u, "\r\n");
}

static const uint8_t macAddrReg[] = {MAADR1, MAADR2, MAADR3, MAADR4, MAADR5, MAADR6};

static const uint8_t phyReg[] = {0x00, 0x01, 0x02, 0x03, 0x10, 0x11, 0x12, 0x13, 0x14};

static void enc28j60_show_macAddr(void)
{
	uint8_t tmp;
	uint8_t i;
	uint8_t mac_addr[6];
	char str[3];

	Enc28J60_GetMacAddr(eth, mac_addr);
	USART_Puts(u, "eth hwaddr ");
	for (i = 0; i < 6; i++) {
		tmp = mac_addr[i];
		itoa(tmp, str, 16);
		USART_Puts(u, str);
		if (i < 6 - 1)
			USART_Putc(u, ':');
	}
	USART_Puts(u, "\r\n");
}

static void enc28j60_show_maxFrameLen(void)
{
	uint16_t tmp;
	char str[6];

	tmp = Enc28J60_GetMaxFrameLen(eth);
	itoa(tmp, str, 10);

	USART_Puts(u, "eth max frm len ");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");
}

static void enc28j60_show_rxbufsize(void)
{
	uint16_t rxStart, rxEnd;
	uint16_t rxSize;
	char str[6];

	rxStart = Enc28J60_GetRxStart(eth);
	rxEnd = Enc28J60_GetRxEnd(eth);

	rxSize = rxEnd - rxStart;
	itoa(rxSize, str, 10);

	USART_Puts(u, "eth rx buf size ");
	USART_Puts(u, str);
	USART_Puts(u, " bytes ");

	USART_Puts(u, "(");
	itoa(rxStart, str, 10);
	USART_Puts(u, str);
	USART_Puts(u, " - ");
	itoa(rxEnd, str, 10);
	USART_Puts(u, str);
	USART_Puts(u, ")");
	USART_Puts(u,"\r\n");
}

static void enc28j60_show_ipgDelayBackToBack(void)
{
	uint8_t ipg;
	char str[3];

	ipg = Enc28J60_GetInterPacketGapDelayForBackToBack(eth);
	itoa(ipg, str, 16),

	USART_Puts(u, "eth IPG B2B 0x");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");
}

static void enc28j60_show_ipgDelayNonBackToBack(void)
{
	uint16_t ipg;
	char str[5];

	ipg = Enc28J60_GetInterPacketGapDelayForNonBackToBack(eth);
	itoa(ipg, str, 16);

	USART_Puts(u, "eth IPG N-B2B 0x");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");
}

static void enc28j60_show_txcrcmode(void)
{
	USART_Puts(u, "eth TX CRC ");
	if (Enc28J60_GetTransmitChecksumEnable(eth))
		USART_Putc(u, '1');
	else
		USART_Putc(u, '0');
	USART_Puts(u, "\r\n");
}

static void enc28j60_show_duplexMode(void)
{
	uint8_t fdpx;

	fdpx = Enc28J60_GetDuplexMode(eth);

	USART_Puts(u, "eth dpx mode ");
	if (fdpx)
		USART_Puts(u, "full");
	else
		USART_Puts(u, "half");
	USART_Puts(u, "-duplex\r\n");
}

static void enc28j60_show_maxRetransmission(void)
{
	char str[3];

	itoa(Enc28J60_GetMaxRetransmission(eth), str, 16);

	USART_Puts(u, "eth RetMax 0x");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");
}

static void enc28j60_show_collisionWindow(void)
{
	char str[3];

	itoa(Enc28J60_GetCollisionWindow(eth), str, 16);

	USART_Puts(u, "eth ColWin 0x");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");
}

static void enc28j60_show_pktcnt(void)
{
	uint8_t pkt_cnt;
	char str[4];

	pkt_cnt = Enc28J60_GetPacketCount(eth);
	itoa(pkt_cnt, str, 10);
	USART_Puts(u, "eth pkt cnt ");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");
}

static void enc28j60_dump(void)
{
	enc28j60_show_receptionEnable();
	enc28j60_show_macReceptionEnable();
	enc28j60_show_macAddr();
	enc28j60_show_maxFrameLen();
	enc28j60_show_rxbufsize();
	enc28j60_show_ipgDelayBackToBack();
	enc28j60_show_ipgDelayNonBackToBack();
	enc28j60_show_txcrcmode();
	enc28j60_show_duplexMode();
	enc28j60_show_maxRetransmission();
	enc28j60_show_collisionWindow();

#if 0
	USART_Puts(u, "PHY Registers\r\n");
	for (i = 0; i < 9; i++) {
		tmp16 = Enc28J60_IO_Rpr(eth_io, phyReg[i]);
		itoa(tmp16, str, 16);
		USART_Puts(u, "reg n 0x");
		USART_Puts(u, str);
		USART_Puts(u, "\r\n");
	}
#endif
}

int main(void)
{
	const uint16_t maxFrameSize = 1518;
	const uint16_t rxBufSize = ((maxFrameSize + 8) * 2);
	const uint8_t macaddr[] = {0x00, 0x22, 0xF9, 0x01, 0x75, 0xAA};
	const uint8_t full_duplex = 1;

	USART_Init(BAUD);
	LED_Init();
	SPI_Master_Init();
	enc28j60_create();

	USART_Puts(u, "eth rst\r\n");

	Enc28J60_IO_Src(eth_io);
	/* (6.4) wait for oscillator to stabilize (ESTAT.CLKRDY) */
	while (!Enc28J60_IsClockReady(eth))
		;
	USART_Puts(u, "eth clkrdy\r\n");

	print_separatorLine();
	USART_Puts(u, "eth before init\r\n");
	enc28j60_dump();

	Enc28J60_Init(eth, rxBufSize, full_duplex, macaddr, maxFrameSize);

	print_separatorLine();
	USART_Puts(u, "eth after init\r\n");
	enc28j60_dump();

	while (1)
	{
		_delay_ms(1000);
		enc28j60_show_pktcnt();
	}
}
