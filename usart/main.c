/* Written for Rainbowduino (ATmega328p at 16MHz) */

#include "USART.h"
#include <avr/io.h>

uint8_t getline(USART self, char *line, const uint8_t max)
{
	uint8_t i = 0;
	char c;
	do {
		c = USART_Getc(self);
		if (c != '\r') {
			USART_Putc(self, c);
			line[i++] = c;
		}
	} while (i < max - 1 && c != '\r');
	line[i] = '\0';
	if (i > 0)
		USART_Puts(self, "\r\n");
	return i;
}

int main(void)
{
	char line[80];

	USART u = USART_Create(&UCSR0A, &UCSR0B, &UCSR0C, &UBRR0L, &UBRR0H, &UDR0);
	USART_SetBaudRate(u, 38400, F_CPU);
	USART_Enable(u, 1);

	USART_Puts(u, "USART Ready\r\n");

	while (1)
	{
		USART_Puts(u, "> ");
		getline(u, line, sizeof(line));
		USART_Puts(u, line);
		USART_Puts(u, "\r\n");
	}

	USART_Destroy(u);
	return 0;
}
