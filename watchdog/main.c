#define BAUD 38400
#define TIMER_PRESCALER		64
#define TIMER_RELOAD_VALUE	(255 - 250)
#define WD_PRESCALER 1024

#include "USART.h"
#include "Reset.h"
#include "Watchdog.h"
#include "Timer.h"
#include "Sched.h"
#include "Uptime.h"
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

static USART u;
static Timer t;

static void USART_Init(const uint32_t baud)
{
	u = USART_Create(&UCSR0A, &UCSR0B, &UCSR0C, &UBRR0L, &UBRR0H, &UDR0);
	USART_SetBaudRate(u, baud, F_CPU);
	USART_Enable(u, 1);
	USART_Puts(u, "USART\r\n");
}

static void Read_RstCause(void)
{
	Reset_Create(&MCUSR);

	USART_Puts(u, "Reset cause:");
	if (Reset_WasCausedByWatchdog())
		USART_Puts(u, " watchdog");
	if (Reset_WasCausedByBrownOut())
		USART_Puts(u, " brown-out");
	if (Reset_WasCausedByExternalReset())
		USART_Puts(u, " ext-reset");
	if (Reset_WasCausedByPowerOnReset())
		USART_Puts(u, " power-on-reset");
	USART_Puts(u, "\r\n");
}

static void WD_Init(void)
{
	Watchdog_Create(1, &WDTCSR);
	Watchdog_SetPrescaler(WD_PRESCALER);
	Watchdog_Enable(1);
	USART_Puts(u, "WDT\r\n");
}

static void WD_Reset(void)
{
	wdt_reset();
}

static void Timer_Init(void)
{
	t = Timer_Create(1, 2, &TCCR2A, &TCCR2B,
						&TCNT2, 0,
						&OCR2A, 0,
						&OCR2B, 0,
						0, 0,
						&TIMSK2, &TIFR2);
	Timer_SetOverflowInterrupt(t, 1);
	Timer_SetCounter(t, TIMER_RELOAD_VALUE);
	Timer_SetPrescaler(t, TIMER_PRESCALER);
	USART_Puts(u, "Timer\r\n");
}

static void Uptime_Handler(void)
{
	char str[9];
	Uptime_Tick();
	Uptime_ToStr(str);
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");
}

static void Sched_Init(void)
{
	Sched_Create();
	Sched_AddTask(WD_Reset, 8000, 0);
	Sched_AddTask(Uptime_Handler, 1000, 0);
	USART_Puts(u, "Sched\r\n");
}

static void Irq_Init(void)
{
	sei();
	USART_Puts(u, "IRQ\r\n");
}

int main(void)
{
	USART_Init(BAUD);
	Read_RstCause();
	Timer_Init();
	Uptime_Create();
	Sched_Init();
	WD_Init();
	Irq_Init();
	while (1)
	{
		Sched_Dispatch();
	}
}

ISR(TIMER2_OVF_vect)
{
	Timer_SetCounter(t, TIMER_RELOAD_VALUE);
	Sched_Update();
}
