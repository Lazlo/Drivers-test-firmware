set -x
set -e
ignore_dir=tmp
make=colormake
for fw in `ls -1`; do
	if [ ! -d $fw ]; then
		continue
	fi
	if [ $fw = $ignore_dir ]; then
		continue
	fi
	cd $fw
	pwd
	git diff .
	$make $*
	cd -
done
