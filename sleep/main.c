#define BAUD 38400
#define TIMER 2
#define F_TIMER_OVF_Hz 125
#define TIMER_PRESCALER 1024
/* F_CPU / TIMER_PRESCALER / F_TIMER_OVF_Hz = ticks
 * 16*(10^6)/1024/125 = 125 */
#define TIMER_RELOAD_VALUE (255 - 125)

#include "Pin.h"
#include "USART.h"
#include "Timer.h"
#include "Sleep.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "Stdio.h"

#if defined(__AVR_ATmega8__)
#define UCSR0A	UCSRA
#define UCSR0B	UCSRB
#define UCSR0C	UCSRC
#define UBRR0L	UBRRL
#define UBRR0H	UBRRH
#define UDR0	UDR
#endif

//#define __SIMULAVR__

#if defined(__SIMULAVR__)
#define SIMULAVR_UDR_OUT	(*((volatile uint8_t *)0x20))
#undef UDR0
#define UDR0 SIMULAVR_UDR_OUT
#endif

static Pin p;
static USART u;
static Timer t;

static void Pin_On(Pin p, const uint8_t on)
{
	if (on) {
		Pin_SetDirection(p, 1);
		Pin_Write(p, 0);
	} else {
		Pin_SetDirection(p, 0);
	}
}

static void Pin_Init(void)
{
	p = Pin_Create(1, &DDRC, &PORTC, &PINC);
	Pin_On(p, 0);
}

static void USART_Init(const uint32_t baud)
{
	u = USART_Create(&UCSR0A, &UCSR0B, &UCSR0C, &UBRR0L, &UBRR0H, &UDR0);
	USART_SetBaudRate(u, BAUD, F_CPU);
	USART_Enable(u, 1);
	USART_Puts(u, "USART\r\n");
}

static void Timer_Init(void)
{
	t = Timer_Create(1, 2, &TCCR2A, &TCCR2B,
					&TCNT2, 0,
					&OCR2A, 0,
					&OCR2B, 0,
					0, 0,
					&TIMSK2, &TIFR2);
	Timer_SetOverflowInterrupt(t, 1);
	Timer_SetCounter(t, TIMER_RELOAD_VALUE);
	Timer_SetPrescaler(t, TIMER_PRESCALER);
	USART_Puts(u, "Timer\r\n");
}

static void Timer_CfgDump(const uint8_t n)
{
	uint8_t tmp;
	char str[4];

	if (n == 2)	tmp = TCCR2A;
	else		tmp = TCCR0A;
	itoa(tmp, str, 10);

	USART_Puts(u, "TCCR");
	USART_Putc(u, '0' + n);
	USART_Puts(u, "A ");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");

	if (n == 2)	tmp = TCCR2B;
	else		tmp = TCCR0B;
	itoa(tmp, str, 10);

	USART_Puts(u, "TCCR");
	USART_Putc(u, '0' + n);
	USART_Puts(u, "B ");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");

	if (n == 2)	tmp = TIMSK2;
	else		tmp = TIMSK0;
	itoa(tmp, str, 10);

	USART_Puts(u, "TIMSK");
	USART_Putc(u, '0' + n);
	USART_Puts(u, " ");
	USART_Puts(u, str);
	USART_Puts(u, "\r\n");
}

static void Sleep_Init(void)
{
	Sleep_Create(1, &SMCR);
	USART_Puts(u, "Sleep\r\n");
}

int main(void)
{
	Pin_Init();
	USART_Init(BAUD);
	Timer_Init();
	Timer_CfgDump(TIMER);
	Sleep_Init();
	sei();
	USART_Puts(u, "IRQ\r\n");
	while (1)
	{
		Pin_On(p, 0);
		USART_Puts(u, "zzz\r\n");
		Sleep_SetMode(SLEEP_MODE_PWRSAVE);
		Sleep_SetEnable(1);
		USART_Puts(u, "W!\r\n");
	}
}

static void ovf_handler(void)
{
	Timer_SetCounter(t, TIMER_RELOAD_VALUE);
	Pin_On(p, 1);
	USART_Puts(u, ".");
}

ISR(TIMER2_OVF_vect)
{
	ovf_handler();
}
