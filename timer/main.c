/* Written for Rainbowduino (ATmega328p at 16MHz) */

#include "USART.h"
#include "Timer.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

static USART uart;
static uint16_t ticks = 0;

static void uart_init(const uint32_t baud)
{
	uart = USART_Create(&UCSR0A, &UCSR0B, &UCSR0C, &UBRR0L, &UBRR0H, &UDR0);
	USART_SetBaudRate(uart, 38400, F_CPU);
	USART_Enable(uart, 1);
	USART_Puts(uart, "USART Ready!\r\n");
}

static void timer0_dbg(void)
{
	uint8_t tmp;

	tmp = TCCR0B;
	USART_Puts(uart, "CS = ");
	USART_Putc(uart, '0' + (tmp & ((1 << 2)|(1 << 1)|(1 << 0))));
	USART_Puts(uart, "\r\n");

	tmp = TIMSK0;
	USART_Puts(uart, "TOIE = ");
	USART_Putc(uart, '0' + (tmp & (1 << 0)));
	USART_Puts(uart, "\r\n");
}

static void timer_init(void)
{
	Timer t0;
	t0 = Timer_Create(1, 0, &TCCR0A, &TCCR0B,
				&TCNT0, 0,
				&OCR0A, 0,
				&OCR0B, 0,
				0, 0,
				&TIMSK0, &TIFR0);
	Timer_SetOverflowInterrupt(t0, 1);
	Timer_SetClockSource(t0, 3); /* 64 */
	timer0_dbg();
	USART_Puts(uart, "Timer Ready!\r\n");
}

int main(void)
{
	uart_init(38400);
	timer_init();
	sei();
	USART_Puts(uart, "IRQ enabled\r\n");
	while (1)
	{
		if (ticks == 0)
			USART_Puts(uart, "t\r\n");
		_delay_ms(1);
	}
	USART_Destroy(uart);
	return 0;
}

ISR(TIMER0_OVF_vect)
{
	if (++ticks == 1000)
		ticks = 0;
}
