#include "USART.h"
#include "Reset.h"
#include <avr/io.h>

#define _TO_STR(sym) #sym
#define TO_STR(sym) _TO_STR(sym)

int main(void)
{
#if defined(__AVR_ATmega8__)
	Reset_Create(&MCUCSR);
#elif defined(__AVR_ATmega328P__)
	Reset_Create(&MCUSR);
#else
#error "Lookup name of register that contains reset-cause bits and add a new if defined() macro line."
#endif

	USART u = USART_Create(&UCSR0A, &UCSR0B, &UCSR0C, &UBRR0L, &UBRR0H, &UDR0);
	USART_SetBaudRate(u, 38400, F_CPU);
	USART_Enable(u, 1);
	USART_Puts(u, "USART Ready!\r\n");

	USART_Puts(u, "Built for ");
	USART_Puts(u, TO_STR(MCU));
	USART_Puts(u, "\r\n");

	USART_Puts(u, "Reset cause:");
	if (Reset_WasCausedByWatchdog())
		USART_Puts(u, " watchdog");
	if (Reset_WasCausedByBrownOut())
		USART_Puts(u, " brown-out");
	if (Reset_WasCausedByExternalReset())
		USART_Puts(u, " ext-reset");
	if (Reset_WasCausedByPowerOnReset())
		USART_Puts(u, " power-on-reset");
	USART_Puts(u, "\r\n");

	while (1)
		;

	USART_Destroy(u);
	return 0;
}
